import React, { Component, Fragment } from "react";
import axios from "axios";

export default class FullFund extends Component {
  state = {
    fundCode: this.props.match.params.id,
    fundDetails: null,
  };
  componentDidMount() {
    let { fundCode } = this.state;
    axios
      .get("https://api.kuvera.in/api/v3/funds/" + fundCode + ".json")
      .then((response) => {
        console.log(response.data[0]);
        this.setState({
          fundDetails: response.data[0],
        });
      });
  }
  render() {
    const { fundDetails } = this.state;
    return (
      <Fragment>
        <div className="container">
          <h4 className="text-center m-4 font-weight-bold">Fund Details</h4>
          <table className="table">
            <thead>
              <tr>
                <th scope="col">Code</th>
                <th scope="col">Category</th>
                <th scope="col">Fund Type</th>
                <th scope="col">Fund House</th>
                <th scope="col">Fund Manager</th>
                <th scope="col">Fund Name</th>
              </tr>
            </thead>
            <tbody>
              {
                <tr>
                  <td>{fundDetails && fundDetails.code}</td>
                  <td>{fundDetails && fundDetails.category}</td>
                  <td>{fundDetails && fundDetails.fund_type}</td>
                  <td>{fundDetails && fundDetails.fund_house}</td>
                  <td>{fundDetails && fundDetails.fund_manager}</td>
                  <td>{fundDetails && fundDetails.fund_name}</td>
                </tr>
              }
            </tbody>
          </table>
        </div>
      </Fragment>
    );
  }
}
