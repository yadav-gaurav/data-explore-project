import React, { Component, Fragment } from "react";
import axios from "axios";
import Fund from "./Fund";
class Explore extends Component {
  state = {
    funds: [],
    filterBy: [],
    prevSortingFlag:'',
    currentSortingFlag:'',
  };
  componentWillMount() {
    axios.get("https://api.kuvera.in/api/v3/funds.json").then((response) => {
      this.setState({
        funds: response.data.slice(0,100),
      });
    });
  }
  renderFunds = () => {
    let { funds, filterBy } = this.state;
    console.log(filterBy);
    if (filterBy.length !== 0) {
      let rawData = [...funds];
      filterBy.forEach((filter) => {
        let filterData = rawData.filter((data) => {
          return data[filter[0]] === filter[1];
        });
        rawData = filterData;
      });
      funds = rawData;
    }
    return funds.map((fund) => {
      return <Fund fund={fund} key={fund.code} />;
    });
  };
  handleChange1 = (event) => {
    let { filterBy } = this.state;
    if (filterBy.length !== 0) {
      let newFilter = filterBy.filter((data) => {
        return data.indexOf("category") < 0;
      });
      let categoryFilter = ["category"];
      categoryFilter.push(event.target.value);
      newFilter.push(categoryFilter);
      filterBy = newFilter;
    } else {
      let planFilter = ["category"];
      planFilter.push(event.target.value);
      filterBy.push(planFilter);
    }
    this.setState({
      filterBy: filterBy,
    });
    if (event.target.value === "null") {
      this.handleUnselect("category");
    }
  };
  handleChange2 = (event) => {
    let { filterBy } = this.state;
    if (filterBy.length !== 0) {
      let newFilter = filterBy.filter((data) => {
        return data.indexOf("fund_type") < 0;
      });
      let fundType = ["fund_type"];
      fundType.push(event.target.value);
      newFilter.push(fundType);
      filterBy = newFilter;
    } else {
      let fundType = ["fund_type"];
      fundType.push(event.target.value);
      filterBy.push(fundType);
    }
    this.setState({
      filterBy: filterBy,
    });
    if (event.target.value === "null") {
      this.handleUnselect("fund_type");
    }
  };
  handleChange3 = (event) => {
    let { filterBy } = this.state;
    if (filterBy.length >= 0) {
      let newFilter = filterBy.filter((data) => {
        return data.indexOf("plan") < 0;
      });
      let planFilter = ["plan"];
      planFilter.push(event.target.value);
      newFilter.push(planFilter);
      filterBy = newFilter;
    } else {
      let planFilter = ["plan"];
      planFilter.push(event.target.value);
      filterBy.push(planFilter);
    }
    this.setState({
      filterBy: filterBy,
    });
    if (event.target.value === "null") {
      this.handleUnselect("plan");
    }
  };
  onClick = (value) => {
    const { prevSortingFlag} = this.state;
    if (typeof value === "string") {
       if(prevSortingFlag.length!==0 && prevSortingFlag===value){
         this.descSort(value);
         this.setState({
           prevSortingFlag:''
         })
       }else{
         this.ascSort(value)
         this.setState({
           prevSortingFlag:value
         })
       }
    } else {
      if(prevSortingFlag.length!==0 && prevSortingFlag===value['returns']){
        this.descSort(value);
        this.setState({
          prevSortingFlag:''
        })
      }else{
        this.ascSort(value)
        this.setState({
          prevSortingFlag:value['returns']
        })
      }
    }
  };
  handleUnselect = (key) => {
    let { filterBy } = this.state;
    let newFilter = filterBy.filter((item) => {
      return item[0] !== key;
    });
    this.setState({
      filterBy: newFilter,
    });
  };
  ascSort=(value)=>{

    const {funds}=this.state;
    if (typeof value === "string") {
      const sortFunds = funds.sort(function(a, b) {
        return (a[value]===null)-(b[value]===null) || +(a[value]>b[value])||-(a[value]<b[value]);
    });
      this.setState({
        funds: sortFunds,
      });
    } else {
      const sortFunds = funds.sort(function(a, b) {
        return (a["returns"][value["returns"]]===null)-(b["returns"][value["returns"]]===null) || +(a["returns"][value["returns"]]>b["returns"][value["returns"]])||-(a["returns"][value["returns"]]<b["returns"][value["returns"]]);
    });
      this.setState({
        funds: sortFunds,
      });
    }
  };
  descSort=(value)=>{
    const {funds}=this.state;
    if (typeof value === "string") {
      const sortFunds = funds.sort(function(a, b) {
        return (a[value]===null)-(b[value]===null) || -(a[value]>b[value])||+(a[value]<b[value]);
    });
      this.setState({
        funds: sortFunds,
      });
    } else {
      const sortFunds = funds.sort(function(a, b) {
        return (a["returns"][value["returns"]]===null)-(b["returns"][value["returns"]]===null) || -(a["returns"][value["returns"]]>b["returns"][value["returns"]])||+(a["returns"][value["returns"]]<b["returns"][value["returns"]]);
    });
      this.setState({
        funds: sortFunds,
      });
    }
  }
  render() {
    return (
      <Fragment>
        <h4 className="text-center m-4 font-weight-bold">Funds Explore Page</h4>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">
                <div
                  onClick={() => this.onClick("name")}
                  className="heading mb-4 text-center"
                >
                  Name Of Fund
                </div>
              </th>
              <th scope="col" className="text-center">
                <div
                  onClick={() => this.onClick("category")}
                  className="heading"
                >
                  Category
                </div>
                <div className="drop-down">
                  <select
                    name="filterBy"
                    className="form-control"
                    onChange={this.handleChange1}
                  >
                    <option value="null">Select Filter</option>
                    <option value="Equity">Equity</option>
                    <option value="ELSS">ELSS</option>
                    <option value="Debt - Bonds">Debt - Bonds</option>
                  </select>
                </div>
              </th>
              <th scope="col" className="text-center">
                <div
                  onClick={() => this.onClick("fund_type")}
                  className="heading"
                >
                  Fund Type
                </div>
                <div className="drop-down">
                  <select
                    name="filterBy"
                    className="form-control"
                    onChange={this.handleChange2}
                  >
                    <option value="null">Select Filter</option>
                    <option value="Equity">Equity</option>
                    <option value="Debt">Debt</option>
                    <option value="Hybrid">Hybrid</option>
                  </select>
                </div>
              </th>
              <th scope="col" className="text-center">
                <div onClick={() => this.onClick("plan")} className="heading">
                  Plan
                </div>
                <div className="drop-down">
                  <select
                    name="filterBy"
                    className="form-control"
                    onChange={this.handleChange3}
                  >
                    <option value="null">Select Filter</option>
                    <option value="GROWTH">GROWTH</option>
                    <option value="DIVIDEND ANNUAL">DIVIDEND ANNUAL</option>
                    <option value="DIVIDEND DAILY">DIVIDEND DAILY</option>
                    <option value="DIVIDEND MONTHLY">DIVIDEND MONTHLY</option>
                  </select>
                </div>
              </th>
              <th scope="col">
                <div
                  onClick={() => this.onClick({ returns: "year_1" })}
                  className="heading mb-4 p-2"
                >
                  Year_1
                </div>
              </th>
              <th scope="col">
                <div
                  onClick={() => this.onClick({ returns: "year_3" })}
                  className="heading mb-4 p-2"
                >
                  Year_3
                </div>
              </th>
            </tr>
          </thead>
          <tbody>{this.renderFunds()}</tbody>
        </table>
      </Fragment>
    );
  }
}

export default Explore;
