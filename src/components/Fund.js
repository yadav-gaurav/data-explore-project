import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";

export default class Fund extends Component {
  render() {
    const fund = this.props.fund;
    return (
      <Fragment>
        <tr>
          <td>
            <Link to={{ pathname: "explore/" + fund.code }}>{fund.name}</Link>
          </td>
          <td className="title-width">{fund.category}</td>
          <td className="title-width">{fund.fund_type}</td>
          <td className="title-width">{fund.plan}</td>
          <td>{fund.returns.year_1}</td>
          <td>{fund.returns.year_3}</td>
        </tr>
      </Fragment>
    );
  }
}
